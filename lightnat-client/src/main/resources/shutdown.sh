cd `dirname $0`/../target
target_dir=`pwd`

pid=`ps ax | grep -i 'lightnat-client' | grep ${target_dir} | grep java | grep -v grep | awk '{print $1}'`
if [ -z "$pid" ] ; then
        echo "No lightnat-client running."
        exit -1;
fi

echo "The lightnat-client(${pid}) is running..."

kill ${pid}

echo "Send shutdown request to lightnat-client(${pid}) OK"
