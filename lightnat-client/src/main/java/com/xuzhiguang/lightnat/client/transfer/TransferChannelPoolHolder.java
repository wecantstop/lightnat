package com.xuzhiguang.lightnat.client.transfer;

public class TransferChannelPoolHolder {

    private static TransferChannelPool transferChannelPool;

    public static void set(TransferChannelPool transferChannelPool) {
        TransferChannelPoolHolder.transferChannelPool = transferChannelPool;
    }

    public static TransferChannelPool get() {
        return transferChannelPool;
    }


}
