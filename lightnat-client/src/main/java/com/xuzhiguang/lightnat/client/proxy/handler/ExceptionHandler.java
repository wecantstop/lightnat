package com.xuzhiguang.lightnat.client.proxy.handler;

import com.xuzhiguang.lightnat.common.manager.ProxyChannelManager;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ChannelHandler.Sharable
public class ExceptionHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.error("exception caught", cause);
        ProxyChannelManager.remove(ctx.channel());
        ctx.close();
        super.exceptionCaught(ctx, cause);
    }

}
