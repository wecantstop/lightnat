package com.xuzhiguang.lightnat.client;

import cn.hutool.setting.dialect.Props;
import com.xuzhiguang.lightnat.client.control.ControlClientProperties;
import com.xuzhiguang.lightnat.client.transfer.TransferClientProperties;

public class ClientProperties implements ControlClientProperties, TransferClientProperties {

    private final Props props;

    public ClientProperties() {
        this(null);
    }

    public ClientProperties(String fileName) {

        if (fileName == null) {
            fileName = System.getProperty("client.properties.path", "./client.properties");
        }

        props = new Props(fileName);
    }

    @Override
    public String getControlServerHost() {
        return props.getStr("natclient.server.host", "127.0.0.1");
    }

    @Override
    public Integer getControlServerPort() {
        return props.getInt("natclient.server.control.port", 1003);
    }

    @Override
    public String getControlToken() {
        return props.getStr("natclient.server.token");
    }

    @Override
    public String getTransferServerHost() {
        return props.getStr("natclient.server.host", "127.0.0.1");
    }

    @Override
    public Integer getTransferServerPort() {
        return props.getInt("natclient.server.transfer.port", 1002);
    }

    @Override
    public String getTransferToken() {
        return props.getStr("natclient.server.token");
    }
}
