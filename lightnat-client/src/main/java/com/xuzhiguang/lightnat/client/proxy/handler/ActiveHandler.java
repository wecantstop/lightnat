package com.xuzhiguang.lightnat.client.proxy.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ChannelHandler.Sharable
public class ActiveHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        log.debug("proxy channel active. channel:{}", ctx.channel());

        ctx.channel().config().setAutoRead(false);
        super.channelActive(ctx);
    }

}
