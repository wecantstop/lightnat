package com.xuzhiguang.lightnat.client.control;

public interface ControlClientProperties {

    String getControlServerHost();

    Integer getControlServerPort();

    String getControlToken();
}
