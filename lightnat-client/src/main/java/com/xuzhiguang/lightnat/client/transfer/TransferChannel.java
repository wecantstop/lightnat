package com.xuzhiguang.lightnat.client.transfer;

import io.netty.channel.Channel;
import lombok.Data;


@Data
public class TransferChannel {

    private Channel channel;

    private volatile Long lastFreeTime;

}
