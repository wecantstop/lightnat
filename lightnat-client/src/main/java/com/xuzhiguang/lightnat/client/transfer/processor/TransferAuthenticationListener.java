package com.xuzhiguang.lightnat.client.transfer.processor;

import io.netty.channel.Channel;

public interface TransferAuthenticationListener {

    void complete(Channel channel, boolean success, String errMsg);
}
