package com.xuzhiguang.lightnat.client.transfer.handler;

import com.xuzhiguang.lightnat.client.transfer.TransferChannelPoolHolder;
import com.xuzhiguang.lightnat.common.manager.TransferChannelManager;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ChannelHandler.Sharable
public class InactiveHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {

        log.debug("transfer channel inactive. channel:{}", ctx.channel());

        TransferChannelManager.remove(ctx.channel());
        TransferChannelPoolHolder.get().remove(ctx.channel());
        super.channelInactive(ctx);
    }
}
