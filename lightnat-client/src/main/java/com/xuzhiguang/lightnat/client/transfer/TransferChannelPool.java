package com.xuzhiguang.lightnat.client.transfer;

import io.netty.channel.Channel;

public interface TransferChannelPool {

    Channel getChannel(long timeout);

    void recycle(Channel channel);

    void remove(Channel channel);

}
