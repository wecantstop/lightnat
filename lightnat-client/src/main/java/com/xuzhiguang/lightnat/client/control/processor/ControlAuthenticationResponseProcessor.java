package com.xuzhiguang.lightnat.client.control.processor;

import com.xuzhiguang.lightnat.common.manager.ControlChannelManager;
import com.xuzhiguang.lightnat.common.message.MessageProcessor;
import com.xuzhiguang.lightnat.common.message.NatMessage;
import com.xuzhiguang.lightnat.common.message.control.ControlAuthenticationResponse;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ControlAuthenticationResponseProcessor implements MessageProcessor {

    @Override
    public void process(ChannelHandlerContext ctx, NatMessage natMessage) {

        log.debug("proxy ControlAuthenticationResponseProcessor. channel:{}", ctx.channel());

        if (natMessage.getBody() instanceof ControlAuthenticationResponse) {

            ControlAuthenticationResponse response = (ControlAuthenticationResponse) natMessage.getBody();

            if (response.getSuccess()) {
                ControlChannelManager.setAuthed(ctx.channel(), true);
                log.info("control client auth success.");
            } else {
                log.warn("control client auth fail. error msg:{}", response.getErrMsg());
                System.err.println("control client auth fail. error msg:" + response.getErrMsg());
                System.exit(0);
            }

        }
    }
}
