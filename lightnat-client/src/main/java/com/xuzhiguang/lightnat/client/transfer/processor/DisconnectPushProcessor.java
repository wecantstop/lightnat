package com.xuzhiguang.lightnat.client.transfer.processor;

import com.xuzhiguang.lightnat.client.transfer.TransferChannelPoolHolder;
import com.xuzhiguang.lightnat.common.manager.ProxyChannelManager;
import com.xuzhiguang.lightnat.common.manager.TransferChannelManager;
import com.xuzhiguang.lightnat.common.message.MessageProcessor;
import com.xuzhiguang.lightnat.common.message.NatMessage;
import com.xuzhiguang.lightnat.common.message.transfer.DisconnectPush;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DisconnectPushProcessor implements MessageProcessor {

    @Override
    public void process(ChannelHandlerContext ctx, NatMessage natMessage) {

        log.debug("transfer DisconnectPushProcessor. channel:{}", ctx.channel());

        if (natMessage.getBody() instanceof DisconnectPush) {
            Long sessionId = TransferChannelManager.getSessionId(ctx.channel());
            if (sessionId == null) {
                log.warn("sessionId is null. channel:{}", ctx.channel());
                return;
            }

            // 回收 transfer channel
            TransferChannelManager.setSessionId(ctx.channel(), null);
            TransferChannelPoolHolder.get().recycle(ctx.channel());

            Channel proxyChannel = ProxyChannelManager.get(sessionId);
            if (proxyChannel == null) {
                log.warn("proxyChannel is null. sessionId:{}", sessionId);
                return;
            }

            // 关闭 proxy channel
            proxyChannel.writeAndFlush(Unpooled.EMPTY_BUFFER);
            ProxyChannelManager.setSyncDisconnect(proxyChannel, false);
            proxyChannel.close();
        }
    }
}
