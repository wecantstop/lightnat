package com.xuzhiguang.lightnat.client.common.handler;

import io.netty.handler.timeout.IdleStateHandler;

public class IdleCheckHandler extends IdleStateHandler {
    public IdleCheckHandler() {
        super(0, 0, 30);
    }
}
