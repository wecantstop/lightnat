package com.xuzhiguang.lightnat.client;

public interface Client {

    void start();

    void stop();
}
