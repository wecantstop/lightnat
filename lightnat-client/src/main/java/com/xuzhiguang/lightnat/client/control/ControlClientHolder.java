package com.xuzhiguang.lightnat.client.control;

public class ControlClientHolder {

    private static ControlClient controlClient;

    public static void set(ControlClient controlClient) {
        ControlClientHolder.controlClient = controlClient;
    }

    public static ControlClient get() {
        return controlClient;
    }

}
