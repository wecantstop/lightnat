package com.xuzhiguang.lightnat.client.proxy;

public class ProxyClientHolder {

    private static ProxyClient proxyClient;

    public static void set(ProxyClient proxyClient) {
        ProxyClientHolder.proxyClient = proxyClient;
    }

    public static ProxyClient get() {
        return proxyClient;
    }

}
