package com.xuzhiguang.lightnat.client.transfer;

public interface TransferClientProperties {

    String getTransferServerHost();

    Integer getTransferServerPort();

    String getTransferToken();
}
