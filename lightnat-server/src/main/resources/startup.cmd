@echo off

if not exist "%JAVA_HOME%\bin\java.exe" echo Please set the JAVA_HOME variable in your environment, We need java(x64)! jdk8 or later is better! & EXIT /B 1
set "JAVA=%JAVA_HOME%\bin\java.exe"

setlocal enabledelayedexpansion

set BASE_DIR=%~dp0
rem added double quotation marks to avoid the issue caused by the folder names containing spaces.
rem removed the last 5 chars(which means \bin\) to get the base DIR.
set BASE_DIR="%BASE_DIR:~0,-5%"

set CUSTOM_SEARCH_LOCATIONS=file:%BASE_DIR%/conf/

set APP=lightnat-server

set "JAVA_OPT=%JAVA_OPT% -Xms512m -Xmx512m -Xmn256m -Xss1M -XX:MetaspaceSize=128M -XX:MaxMetaspaceSize=128M"

set "JAVA_OPT=%JAVA_OPT% -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:PretenureSizeThreshold=20M -XX:CMSInitiatingOccupancyFraction=92 "
set "JAVA_OPT=%JAVA_OPT% -XX:+CMSParallelInitialMarkEnabled -XX:+CMSScavengeBeforeRemark -XX:+DisableExplicitGC"
set "JAVA_OPT=%JAVA_OPT% -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=%BASE_DIR%/logs/java_heapdump.hprof"
set "JAVA_OPT=%JAVA_OPT% -Xloggc:%BASE_DIR%/logs/gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M"


set "JAVA_OPT=%JAVA_OPT% -Dnatserver.home=%BASE_DIR%"
set "JAVA_OPT=%JAVA_OPT% -jar %BASE_DIR%/target/%APP%.jar"

set "JAVA_OPT=%JAVA_OPT% --spring.config.additional-location=%CUSTOM_SEARCH_LOCATIONS%"
set "JAVA_OPT=%JAVA_OPT% --logging.config=%BASE_DIR%/conf/logback-spring.xml"



if not exist %BASE_DIR%/logs (
    md %BASE_DIR%/logs
)


set COMMAND="%JAVA%" %JAVA_OPT% lightnat-server %*

%COMMAND%
