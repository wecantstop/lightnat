package com.xuzhiguang.lightnat.server.core.server.proxy.refuse;

import io.netty.channel.Channel;

public class GeneralRefuseStrategy implements RefuseStrategy {
    @Override
    public void refuse(Channel channel, String msg) {
        channel.close();
    }
}
