package com.xuzhiguang.lightnat.server.wrap.properties;

import com.xuzhiguang.lightnat.server.core.server.control.ControlServerProperties;
import com.xuzhiguang.lightnat.server.core.server.transfer.TransferServerProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("natserver")
public class ServerProperties implements ControlServerProperties, TransferServerProperties {

    private Control control;

    private Transfer transfer;

    @Data
    public static class Transfer {

        private String bindHost = "*";

        private Integer bindPort = 1002;

    }

    @Data
    public static class Control {

        private String bindHost = "*";

        private Integer bindPort = 1003;

    }

    @Override
    public String getControlBindHost() {
        return control.getBindHost();
    }

    @Override
    public Integer getControlBindPort() {
        return control.getBindPort();
    }

    @Override
    public String getTransferBindHost() {
        return transfer.getBindHost();
    }

    @Override
    public Integer getTransferBindPort() {
        return transfer.getBindPort();
    }
}
