package com.xuzhiguang.lightnat.server.core.server.common.processor;

import com.xuzhiguang.lightnat.common.message.MessageProcessor;
import com.xuzhiguang.lightnat.common.message.NatMessage;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KeepAliveRequestProcessor implements MessageProcessor {

    @Override
    public void process(ChannelHandlerContext ctx, NatMessage natMessage) {
    }
}
