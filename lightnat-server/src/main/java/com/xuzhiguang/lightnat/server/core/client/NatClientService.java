package com.xuzhiguang.lightnat.server.core.client;

import com.xuzhiguang.lightnat.server.core.exception.NatClientNotFoundException;
import com.xuzhiguang.lightnat.server.core.exception.NatProxyNotFoundException;

import java.util.List;

public interface NatClientService {

    NatClient getNatClient(String token);

    NatClient getNatClient(long clientId);

    NatProxy getNatProxy(String proxyHost, Integer proxyPort);

    NatProxy getNatProxy(long proxyId);

    List<NatClient> getAllNatClients();

    List<NatProxy> getNatProxies(long clientId) throws NatClientNotFoundException;

    List<NatProxy> getAllProxies();

    void addNatClient(NatClient natClient);

    NatClient modifyNatClient(long clientId, NatClient natClient) throws NatClientNotFoundException;

    void addNatProxy(NatProxy natProxy) throws NatClientNotFoundException;

    NatProxy modifyNatProxy(long proxyId, NatProxy natProxy) throws NatProxyNotFoundException;

    void deleteNatClient(long clientId);

    void deleteNatProxy(long proxyId);


}
