package com.xuzhiguang.lightnat.server.core.server.control;

public interface ControlServerProperties {

    String getControlBindHost();

    Integer getControlBindPort();
}
