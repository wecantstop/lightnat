package com.xuzhiguang.lightnat.server.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProxyGetResponse {

    private Long id;

    private Long clientId;

    private String proxyType;

    private String proxyHost;

    private Integer proxyPort;

    private String sourceHost;

    private Integer sourcePort;

}
