package com.xuzhiguang.lightnat.server.api.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xuzhiguang.lightnat.server.api.domain.ApiResponse;
import com.xuzhiguang.lightnat.server.api.domain.ResponseEnum;
import com.xuzhiguang.lightnat.server.api.util.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Configuration
public class AuthConfig extends WebSecurityConfigurerAdapter {

    private final ObjectMapper objectMapper;

    public AuthConfig(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                .antMatchers("/api/user/**", "/assets/**", "/index.html")
                .permitAll()
                .anyRequest().authenticated()
                .and()
                .csrf()
                .disable()

                .formLogin()
                .loginProcessingUrl("/api/user/login")
                .loginPage("/")
                .permitAll()
                .successHandler(new CustomAuthenticationSuccessHandler(objectMapper))
                .failureHandler(new CustomAuthenticationFailureHandler(objectMapper))
                .and()
                .httpBasic()
                .authenticationEntryPoint(new CustomAuthenticationEntryPoint(objectMapper));
    }

    private static class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

        private final ObjectMapper objectMapper;

        private CustomAuthenticationFailureHandler(ObjectMapper objectMapper) {
            this.objectMapper = objectMapper;
        }

        @Override
        public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
            ResponseUtils.writeResponse(httpServletResponse, objectMapper, new ApiResponse<>(ResponseEnum.USERNAME_PASSWORD_ERROR));
        }
    }

    private static class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

        private final ObjectMapper objectMapper;

        private CustomAuthenticationSuccessHandler(ObjectMapper objectMapper) {
            this.objectMapper = objectMapper;
        }

        @Override
        public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException {

            ResponseUtils.writeResponse(httpServletResponse, objectMapper, new ApiResponse<>(ResponseEnum.SUCCESS));

        }
    }

    private static class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {
        private final ObjectMapper objectMapper;

        private CustomAuthenticationEntryPoint(ObjectMapper objectMapper) {
            this.objectMapper = objectMapper;
        }

        @Override
        public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
            ResponseUtils.writeResponse(response, objectMapper, new ApiResponse<>(ResponseEnum.NOT_LOGIN));
        }
    }
}
