package com.xuzhiguang.lightnat.server.api.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xuzhiguang.lightnat.server.api.domain.ApiResponse;
import com.xuzhiguang.lightnat.server.api.domain.ResponseEnum;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ResponseUtils {

    public static void writeResponse(HttpServletResponse httpServletResponse, ObjectMapper objectMapper, ApiResponse apiResponse) throws IOException {
        httpServletResponse.setContentType("application/json;charset=utf-8");

        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        String s = objectMapper.writeValueAsString(apiResponse);
        try (PrintWriter writer = httpServletResponse.getWriter()) {
            writer.write(s);
            writer.flush();
        }
    }
}
