package com.xuzhiguang.lightnat.server.api.controller;

import com.xuzhiguang.lightnat.server.api.domain.ApiResponse;
import com.xuzhiguang.lightnat.server.api.domain.ResponseEnum;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/api/user")
public class UserController {

    @GetMapping("/check")
    public ApiResponse<?> check(Authentication authentication) {

        if (authentication != null && authentication.isAuthenticated()) {
            return new ApiResponse<>(ResponseEnum.SUCCESS);
        } else {
            return new ApiResponse<>(ResponseEnum.NOT_LOGIN);
        }
    }

}
