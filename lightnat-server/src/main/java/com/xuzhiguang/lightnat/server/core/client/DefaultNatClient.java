package com.xuzhiguang.lightnat.server.core.client;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class DefaultNatClient implements NatClient {

    private Long id;

    private String token;

    private String name;

    private Long expireTime;

    private List<DefaultNatProxy> proxies = new LinkedList<>();




}
