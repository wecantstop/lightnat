package com.xuzhiguang.lightnat.server.wrap.server;

import com.xuzhiguang.lightnat.server.core.server.Server;
import lombok.Data;
import org.springframework.context.SmartLifecycle;

@Data
public class ServerBean implements SmartLifecycle {

    private final Server server;

    private volatile boolean isRunning = false;

    public ServerBean(Server server) {
        this.server = server;
    }

    @Override
    public void start() {
        try {
            this.server.start();
            this.isRunning = true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void stop() {
        try {
            this.server.stop();
            this.isRunning = false;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isRunning() {
        return this.isRunning;
    }
}
