package com.xuzhiguang.lightnat.server.core.server.control.handler;

import com.xuzhiguang.lightnat.common.manager.ControlChannelManager;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ChannelHandler.Sharable
public class InactiveHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {

        log.debug("control channel inactive. channel:{}", ctx.channel());

        ControlChannelManager.remove(ctx.channel());
        ctx.close();
        super.channelInactive(ctx);
    }
}
