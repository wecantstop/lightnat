package com.xuzhiguang.lightnat.server.core.client;

public interface NatClient {

    Long getId();

    String getToken();

    String getName();

    Long getExpireTime();

}
