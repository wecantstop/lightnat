package com.xuzhiguang.lightnat.server.core.client;

public enum ProxyTypeEnum {

    HTTP;

    public static ProxyTypeEnum findByName(String name) {

        for (ProxyTypeEnum value : ProxyTypeEnum.values()) {
            if (value.name().equals(name)) {
                return value;
            }
        }
        return null;

    }

}
