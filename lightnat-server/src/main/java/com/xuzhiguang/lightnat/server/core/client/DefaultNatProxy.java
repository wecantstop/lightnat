package com.xuzhiguang.lightnat.server.core.client;

import lombok.Data;

@Data
public class DefaultNatProxy implements NatProxy {

    private Long id;

    private Long clientId;

    private ProxyTypeEnum proxyType;

    private String proxyHost;

    private Integer proxyPort;

    private String sourceHost;

    private Integer sourcePort;

    private Long limit;

}
