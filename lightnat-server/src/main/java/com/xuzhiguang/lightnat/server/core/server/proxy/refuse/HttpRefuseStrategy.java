package com.xuzhiguang.lightnat.server.core.server.proxy.refuse;

import cn.hutool.core.util.ArrayUtil;
import io.netty.channel.Channel;

import java.nio.charset.StandardCharsets;

public class HttpRefuseStrategy implements RefuseStrategy {
    @Override
    public void refuse(Channel channel, String msg) {

        byte[] msgData = msg.getBytes(StandardCharsets.UTF_8);

        String header = "HTTP/1.1 400 Bad Request\n" +
                "Content-Type: text/plain\n" +
                "content-length: " + msgData.length + "\n" +
                "connection: close\n" +
                "\n";
        byte[] data = ArrayUtil.addAll(header.getBytes(StandardCharsets.UTF_8), msgData);
        channel.writeAndFlush(data);
        channel.close();
    }


}
