package com.xuzhiguang.lightnat.server.core.server.proxy.refuse;

import com.xuzhiguang.lightnat.server.core.client.ProxyTypeEnum;

public class RefuseStrategyFactory {

    private final static GeneralRefuseStrategy GENERAL_REFUSE_STRATEGY = new GeneralRefuseStrategy();

    private final static HttpRefuseStrategy HTTP_REFUSE_STRATEGY = new HttpRefuseStrategy();

    public static RefuseStrategy getStrategy(ProxyTypeEnum proxyTypeEnum) {

        if (ProxyTypeEnum.HTTP.equals(proxyTypeEnum)) {
            return HTTP_REFUSE_STRATEGY;
        } else {
            return GENERAL_REFUSE_STRATEGY;
        }
    }

}
