package com.xuzhiguang.lightnat.server.core.server.proxy.refuse;

import io.netty.channel.Channel;

public interface RefuseStrategy {

    void refuse(Channel channel, String msg);
}
