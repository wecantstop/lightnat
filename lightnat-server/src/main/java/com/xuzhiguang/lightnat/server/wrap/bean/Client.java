package com.xuzhiguang.lightnat.server.wrap.bean;

import lombok.Data;

@Data
public class Client {

    private Long id;

    private String token;

    private String name;

    private Long expireTime;

}
