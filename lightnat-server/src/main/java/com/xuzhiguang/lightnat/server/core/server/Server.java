package com.xuzhiguang.lightnat.server.core.server;

public interface Server {

    void start() throws InterruptedException;

    void stop();

}
