package com.xuzhiguang.lightnat.server.core.server.control;

import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.StrUtil;
import com.xuzhiguang.lightnat.common.codec.NatFrameDecoder;
import com.xuzhiguang.lightnat.common.codec.NatFrameEncoder;
import com.xuzhiguang.lightnat.common.codec.NatMessageDecoder;
import com.xuzhiguang.lightnat.common.codec.NatMessageEncoder;
import com.xuzhiguang.lightnat.common.message.MessageProcessorFactory;
import com.xuzhiguang.lightnat.common.message.MessageProcessorHandler;
import com.xuzhiguang.lightnat.server.core.client.NatClientService;
import com.xuzhiguang.lightnat.server.core.server.Server;
import com.xuzhiguang.lightnat.server.core.server.control.handler.AuthenticationHandler;
import com.xuzhiguang.lightnat.server.core.server.control.handler.ExceptionHandler;
import com.xuzhiguang.lightnat.server.core.server.control.handler.IdleCheckHandler;
import com.xuzhiguang.lightnat.server.core.server.control.handler.InactiveHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.DefaultThreadFactory;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class ControlServer implements Server {

    private NatClientService natClientService;

    private ControlServerProperties controlServerProperties;

    private MessageProcessorFactory messageProcessorFactory;

    private EventLoopGroup bossGroup;

    private EventLoopGroup workerGroup;

    private Channel channel;

    private ControlServer() {
    }

    public static class Builder {

        private final ControlServer controlServer;

        public Builder() {
            this.controlServer = new ControlServer();
        }

        public Builder natClientService(NatClientService natClientService) {
            this.controlServer.setNatClientService(natClientService);
            return this;
        }

        public Builder controlServerProperties(ControlServerProperties controlServerProperties) {
            this.controlServer.setControlServerProperties(controlServerProperties);
            return this;
        }

        public Builder messageProcessorFactory(MessageProcessorFactory messageProcessorFactory) {
            this.controlServer.setMessageProcessorFactory(messageProcessorFactory);
            return this;
        }

        public ControlServer build() {
            return this.controlServer;
        }
    }

    @Override
    public void start() throws InterruptedException {

        checkProperties();

        bossGroup = new NioEventLoopGroup(1, new DefaultThreadFactory("c-boss"));
        workerGroup = new NioEventLoopGroup(new DefaultThreadFactory("c-worker"));
        ServerBootstrap bootstrap = new ServerBootstrap();

        LoggingHandler loggingHandler = new LoggingHandler(LogLevel.INFO);
        AuthenticationHandler authenticationHandler = new AuthenticationHandler(natClientService);
        MessageProcessorHandler messageProcessorHandler = new MessageProcessorHandler(this.messageProcessorFactory);
        InactiveHandler inactiveHandler = new InactiveHandler();
        ExceptionHandler exceptionHandler = new ExceptionHandler();

        bootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_REUSEADDR, true)
                .childOption(ChannelOption.TCP_NODELAY, true)
                .handler(loggingHandler)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline()
                                .addLast("idleCheckHandler", new IdleCheckHandler())
                                .addLast("natFrameDecoder", new NatFrameDecoder())
                                .addLast("natFrameEncoder", new NatFrameEncoder())
                                .addLast("natMessageDecoder", new NatMessageDecoder())
                                .addLast("natMessageEncoder", new NatMessageEncoder())
                                .addLast("authHandler", authenticationHandler)
                                .addLast("messageProcessorHandler", messageProcessorHandler)
                                .addLast("inactiveHandler", inactiveHandler)
                                .addLast("exceptionHandler", exceptionHandler);
                    }
                });

        ChannelFuture f;
        if ("*".equals(controlServerProperties.getControlBindHost())) {
            f = bootstrap.bind(controlServerProperties.getControlBindPort()).sync();
        } else {
            f = bootstrap.bind(controlServerProperties.getControlBindHost(), controlServerProperties.getControlBindPort()).sync();
        }
        channel = f.channel();
        log.info("control server started success. host:{}, port:{}",
                controlServerProperties.getControlBindHost(), controlServerProperties.getControlBindPort());
    }

    @Override
    public void stop() {
        channel.close();
        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
    }

    private void checkProperties() {
        if (StrUtil.isBlank(controlServerProperties.getControlBindHost())) {
            throw new IllegalArgumentException("server host is black.");
        }
        if (!NetUtil.isValidPort(controlServerProperties.getControlBindPort())) {
            throw new IllegalArgumentException("server port is illegal. server port:" + controlServerProperties.getControlBindPort());
        }

    }
}
