package com.xuzhiguang.lightnat.server.core.server.control.handler;

import com.xuzhiguang.lightnat.common.manager.ControlChannelManager;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ChannelHandler.Sharable
public class ExceptionHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.error("exception caught", cause);
        ControlChannelManager.remove(ctx.channel());
        ctx.close();
        super.exceptionCaught(ctx, cause);
    }

}
