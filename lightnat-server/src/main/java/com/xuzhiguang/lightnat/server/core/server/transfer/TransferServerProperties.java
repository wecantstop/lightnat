package com.xuzhiguang.lightnat.server.core.server.transfer;

public interface TransferServerProperties {

    String getTransferBindHost();

    Integer getTransferBindPort();

}
