package com.xuzhiguang.lightnat.server.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class NatException extends Exception {

    private Integer code;

    public NatException(String message, Throwable cause) {
        super(message, cause);
        this.code = getCode();
    }

    public NatException(String message) {
        super(message);
        this.code = getCode();
    }

    public abstract int getCode();
}
