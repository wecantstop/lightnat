package com.xuzhiguang.lightnat.server.core.server.transfer.processor;

import com.xuzhiguang.lightnat.common.manager.ProxyChannelManager;
import com.xuzhiguang.lightnat.common.message.MessageProcessor;
import com.xuzhiguang.lightnat.common.message.NatMessage;
import com.xuzhiguang.lightnat.common.message.transfer.DisconnectRequest;
import com.xuzhiguang.lightnat.server.core.server.transfer.TransferChannelManager;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DisconnectRequestProcessor implements MessageProcessor {
    @Override
    public void process(ChannelHandlerContext ctx, NatMessage natMessage) {

        log.debug("transfer DisconnectRequestProcessor. channel:{}", ctx.channel());

        if (natMessage.getBody() instanceof DisconnectRequest) {
            Long sessionId = TransferChannelManager.getSessionId(ctx.channel());
            if (sessionId == null) {
                log.warn("sessionId is null. channel:{}", ctx.channel());
                return;
            }
            Channel proxyChannel = ProxyChannelManager.get(sessionId);
            if (proxyChannel == null) {
                log.warn("proxyChannel is null. sessionId:{}", sessionId);
                return;
            }

            log.info("transfer disconnect. sessionId:{}", sessionId);

            ProxyChannelManager.setSyncDisconnect(proxyChannel, false);
            proxyChannel.writeAndFlush(Unpooled.EMPTY_BUFFER);
            proxyChannel.close();
        }
    }
}
