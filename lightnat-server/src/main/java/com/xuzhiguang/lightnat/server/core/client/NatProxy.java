package com.xuzhiguang.lightnat.server.core.client;

public interface NatProxy {

    Long getId();

    Long getClientId();

    ProxyTypeEnum getProxyType();

    String getProxyHost();

    Integer getProxyPort();

    String getSourceHost();

    Integer getSourcePort();

    Long getLimit();

}
