package com.xuzhiguang.lightnat.server.core.exception;

public class NatClientNotFoundException extends NatException {

    public NatClientNotFoundException(String message) {
        super(message);
    }

    @Override
    public int getCode() {
        return NatExceptionEnum.NAT_CLIENT_NOT_FOUND.getCode();
    }
}
