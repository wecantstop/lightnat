package com.xuzhiguang.lightnat.server.wrap.bean;

import com.xuzhiguang.lightnat.server.core.client.ProxyTypeEnum;
import lombok.Data;

@Data
public class Proxy {

    private Long id;

    private Long clientId;

    private ProxyTypeEnum proxyType;

    private String proxyHost;

    private Integer proxyPort;

    private String sourceHost;

    private Integer sourcePort;

}
