package com.xuzhiguang.lightnat.server.core.exception;

public class NatProxyNotFoundException extends NatException {

    public NatProxyNotFoundException(String message) {
        super(message);
    }

    @Override
    public int getCode() {
        return NatExceptionEnum.NAT_PROXY_NOT_FOUND.getCode();
    }
}
