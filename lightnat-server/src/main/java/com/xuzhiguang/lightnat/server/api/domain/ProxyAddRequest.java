package com.xuzhiguang.lightnat.server.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProxyAddRequest {

    @NotNull
    private Long clientId;

    @NotEmpty
    private String proxyType;

    @NotEmpty
    private String proxyHost;

    @NotNull
    private Integer proxyPort;

    @NotEmpty
    private String sourceHost;

    @NotNull
    private Integer sourcePort;

}
