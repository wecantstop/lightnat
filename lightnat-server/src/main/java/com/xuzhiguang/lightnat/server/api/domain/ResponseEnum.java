package com.xuzhiguang.lightnat.server.api.domain;

public enum ResponseEnum {


    SUCCESS(0, "success"),

    SYSTEM_ERROR(-1, "system error"),

    PARAMETERS_VALIDATE_ERROR(501, "parameters validate error"),

    USERNAME_PASSWORD_ERROR(401, "用户名或密码错误"),

    NOT_LOGIN(402, "未登录");

    private final int code;

    private final String msg;


    ResponseEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
