package com.xuzhiguang.lightnat.server.core.exception;

public enum NatExceptionEnum {

    NAT_CLIENT_NOT_FOUND(100),

    NAT_PROXY_NOT_FOUND(101);

    private final int code;

    NatExceptionEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
