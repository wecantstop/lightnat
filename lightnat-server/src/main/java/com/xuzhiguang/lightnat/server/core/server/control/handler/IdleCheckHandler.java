package com.xuzhiguang.lightnat.server.core.server.control.handler;

import com.xuzhiguang.lightnat.common.manager.ControlChannelManager;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IdleCheckHandler extends IdleStateHandler {
    public IdleCheckHandler() {
        super(0, 0, 90);
    }

    @Override
    protected void channelIdle(ChannelHandlerContext ctx, IdleStateEvent evt) throws Exception {

        log.debug("control channel idle. channel:{}", ctx.channel());

        if (evt.equals(IdleStateEvent.FIRST_ALL_IDLE_STATE_EVENT)) {
            log.warn("channel idle, close the channel. channel:{}", ctx.channel());
            ControlChannelManager.remove(ctx.channel());
            ctx.close();
        }
        super.channelIdle(ctx, evt);
    }
}
