package com.xuzhiguang.lightnat.common.constant;

import io.netty.util.AttributeKey;

public class AttributeKeyConstant {

    public static final AttributeKey<Long> proxyIdKey = AttributeKey.valueOf("proxyId");

    public static final AttributeKey<Long> sessionIdKey = AttributeKey.valueOf("sessionId");

    public static final AttributeKey<Long> clientIdKey = AttributeKey.valueOf("clientId");

    public static final AttributeKey<Long> transferIdKey = AttributeKey.valueOf("transferId");

    public static final AttributeKey<Boolean> authedKey = AttributeKey.valueOf("authed");

    public static final AttributeKey<Boolean> syncDisconnectKey = AttributeKey.valueOf("syncDisconnect");
}
