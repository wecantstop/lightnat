package com.xuzhiguang.lightnat.common.message.transfer;

import lombok.Data;

@Data
public class ConnectRequest {

    private Long sessionId;
}
