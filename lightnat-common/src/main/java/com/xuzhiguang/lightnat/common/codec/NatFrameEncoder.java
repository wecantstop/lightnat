package com.xuzhiguang.lightnat.common.codec;

import io.netty.handler.codec.LengthFieldPrepender;

public class NatFrameEncoder extends LengthFieldPrepender {
    public NatFrameEncoder() {
        super(4);
    }
}
