package com.xuzhiguang.lightnat.common.message.control;

import lombok.Data;

@Data
public class ConnectPush {

    private Long sessionId;

    private String sourceHost;

    private Integer sourcePort;

}
