package com.xuzhiguang.lightnat.common.manager;

import com.xuzhiguang.lightnat.common.constant.AttributeKeyConstant;
import io.netty.channel.Channel;

import java.util.concurrent.ConcurrentHashMap;

public class TransferChannelManager {


    private static final ConcurrentHashMap<Long, Channel> channelMap = new ConcurrentHashMap<>();

    public static void add(long transferId, Channel channel) {

        channelMap.put(transferId, channel);

        channel.attr(AttributeKeyConstant.transferIdKey).set(transferId);
    }

    public static void remove(Channel channel) {
        Long transferId = getTransferId(channel);
        if (transferId != null) {
            channelMap.remove(transferId);
        }
    }

    public static Long getTransferId(Channel channel) {
        return channel.attr(AttributeKeyConstant.transferIdKey).get();
    }

    public static Channel getChannel(Long transferId) {
        return channelMap.get(transferId);
    }

    public static void setSessionId(Channel channel, Long sessionId) {
        channel.attr(AttributeKeyConstant.sessionIdKey).set(sessionId);
    }

    public static Long getSessionId(Channel channel) {
        return channel.attr(AttributeKeyConstant.sessionIdKey).get();
    }

}
