package com.xuzhiguang.lightnat.common.codec;

import com.xuzhiguang.lightnat.common.message.NatMessage;
import com.xuzhiguang.lightnat.common.message.NatMessageHeader;
import com.xuzhiguang.lightnat.common.serializer.NatMessageSerializer;
import com.xuzhiguang.lightnat.common.serializer.SerializerFactory;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class NatMessageEncoder extends MessageToByteEncoder<NatMessage> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, NatMessage natMessage, ByteBuf byteBuf) throws Exception {

        // encode header
        NatMessageHeader header = natMessage.getHeader();
        byteBuf.writeShort(header.getMagic());
        byteBuf.writeByte(header.getVersion());
        byteBuf.writeByte(header.getSerializerType());
        byteBuf.writeShort(header.getMessageType());
        byteBuf.writeLong(header.getRequestId());

        // encode body
        NatMessageSerializer serializer = SerializerFactory.getSerializer(header.getSerializerType());
        byte[] bodyData = serializer.serialize(natMessage.getBody());
        byteBuf.writeBytes(bodyData);

    }
}
