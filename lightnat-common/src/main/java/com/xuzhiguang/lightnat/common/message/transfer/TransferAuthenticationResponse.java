package com.xuzhiguang.lightnat.common.message.transfer;

import lombok.Data;

@Data
public class TransferAuthenticationResponse {

    private Boolean success;

    private Long transferId;

    private String errMsg;
}
