package com.xuzhiguang.lightnat.common.message;


import com.xuzhiguang.lightnat.common.message.common.KeepAliveRequest;
import com.xuzhiguang.lightnat.common.message.control.ConnectPush;
import com.xuzhiguang.lightnat.common.message.control.ConnectResponse;
import com.xuzhiguang.lightnat.common.message.control.ControlAuthenticationRequest;
import com.xuzhiguang.lightnat.common.message.control.ControlAuthenticationResponse;
import com.xuzhiguang.lightnat.common.message.transfer.*;

public enum NatMessageTypeEnum {


    TRANSFER((short) 100, byte[].class),

    KEEP_ALIVE_REQUEST((short) 101, KeepAliveRequest.class),


    CONNECT_PUSH((short) 200, ConnectPush.class),

    CONNECT_RESPONSE((short) 201, ConnectResponse.class),

    CONTROL_AUTHENTICATION_REQUEST((short) 202, ControlAuthenticationRequest.class),

    CONTROL_AUTHENTICATION_RESPONSE((short) 203, ControlAuthenticationResponse.class),


    CONNECT_REQUEST((short) 300, ConnectRequest.class),

    DISCONNECT_PUSH((short) 301, DisconnectPush.class),

    DISCONNECT_REQUEST((short) 302, DisconnectRequest.class),

    TRANSFER_AUTHENTICATION_REQUEST((short) 303, TransferAuthenticationRequest.class),

    TRANSFER_AUTHENTICATION_RESPONSE((short) 304, TransferAuthenticationResponse.class);


    private final short type;

    private final Class<?> cls;

    NatMessageTypeEnum(short type, Class<?> cls) {
        this.type = type;
        this.cls = cls;
    }

    public static NatMessageTypeEnum findByType(short type) {
        for (NatMessageTypeEnum value : NatMessageTypeEnum.values()) {
            if (value.type == type) {
                return value;
            }
        }
        return null;
    }

    public static NatMessageTypeEnum findByCls(Class<?> cls) {
        for (NatMessageTypeEnum value : NatMessageTypeEnum.values()) {
            if (value.cls.equals(cls)) {
                return value;
            }
        }
        return null;
    }

    public short getType() {
        return type;
    }

    public Class<?> getCls() {
        return cls;
    }

}
