package com.xuzhiguang.lightnat.common.message;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ChannelHandler.Sharable
public class MessageProcessorHandler extends SimpleChannelInboundHandler<NatMessage> {

    private final MessageProcessorFactory messageProcessorFactory;

    public MessageProcessorHandler(MessageProcessorFactory messageProcessorFactory) {
        this.messageProcessorFactory = messageProcessorFactory;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, NatMessage msg) throws Exception {
        MessageProcessor processor = messageProcessorFactory.get(msg.getHeader().getMessageType());
        if (processor != null) {
            processor.process(ctx, msg);
        }
    }
}
