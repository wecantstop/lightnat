package com.xuzhiguang.lightnat.common.message.control;

import lombok.Data;

@Data
public class ControlAuthenticationRequest {

    private String token;

}
