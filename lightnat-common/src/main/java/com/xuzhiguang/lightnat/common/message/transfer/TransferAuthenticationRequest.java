package com.xuzhiguang.lightnat.common.message.transfer;

import lombok.Data;

@Data
public class TransferAuthenticationRequest {

    private String token;

}
