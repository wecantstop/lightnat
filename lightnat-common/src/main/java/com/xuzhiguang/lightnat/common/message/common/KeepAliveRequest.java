package com.xuzhiguang.lightnat.common.message.common;

import lombok.Data;

@Data
public class KeepAliveRequest {

    private Long time;
}
