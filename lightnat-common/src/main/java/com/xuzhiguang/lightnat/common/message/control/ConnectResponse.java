package com.xuzhiguang.lightnat.common.message.control;

import lombok.Data;

@Data
public class ConnectResponse {

    private Long sessionId;

    private Boolean success;

    private String errMsg;

}
