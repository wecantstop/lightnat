package com.xuzhiguang.lightnat.common.message.control;

import lombok.Data;

@Data
public class ControlAuthenticationResponse {

    private Boolean success;

    private String errMsg;
}
