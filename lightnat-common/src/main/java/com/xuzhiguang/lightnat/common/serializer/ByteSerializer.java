package com.xuzhiguang.lightnat.common.serializer;

public class ByteSerializer implements NatMessageSerializer {
    @Override
    public byte[] serialize(Object obj) {

        if (obj instanceof byte[]) {
            return (byte[]) obj;
        }
        return new byte[0];
    }

    @Override
    public <T> T deserialize(byte[] bytes, Class<T> cls) {

        if (cls.isAssignableFrom(byte[].class)) {
            return (T) bytes;
        }

        return null;
    }

}
