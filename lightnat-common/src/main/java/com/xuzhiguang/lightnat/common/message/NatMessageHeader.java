package com.xuzhiguang.lightnat.common.message;


import lombok.Data;

@Data
public class NatMessageHeader {

    /**
     * 魔数
     */
    private short magic;

    /**
     * 协议版本号
     */
    private byte version;

    /**
     * 序列化算法
     */
    private byte serializerType;

    /**
     * 消息类型
     */
    private short messageType;

    /**
     * 请求id
     */
    private long requestId;


}
