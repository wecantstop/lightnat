package com.xuzhiguang.lightnat.common.serializer;

public enum SerializerTypeEnum {

    GSON((byte) 1),

    BYTE((byte) 2);

    private final byte type;

    SerializerTypeEnum(byte type) {
        this.type = type;
    }

    public static SerializerTypeEnum findByType(byte serializerType) {
        for (SerializerTypeEnum typeEnum : SerializerTypeEnum.values()) {
            if (typeEnum.getType() == serializerType) {
                return typeEnum;
            }
        }
        return null;
    }

    public byte getType() {
        return type;
    }
}
