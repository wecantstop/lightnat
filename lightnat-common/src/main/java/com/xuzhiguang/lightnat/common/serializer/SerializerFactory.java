package com.xuzhiguang.lightnat.common.serializer;

public class SerializerFactory {

    private static class GsonSerializerHolder {
        private final static GsonSerializer INSTANCE = new GsonSerializer();
    }

    private static class ByteSerializerHolder {
        private final static ByteSerializer INSTANCE = new ByteSerializer();
    }

    private SerializerFactory() {}

    public static NatMessageSerializer getSerializer(byte serializerType) {

        if (SerializerTypeEnum.GSON.getType() == serializerType) {
            return GsonSerializerHolder.INSTANCE;
        }

        if (SerializerTypeEnum.BYTE.getType() == serializerType) {
            return ByteSerializerHolder.INSTANCE;
        }

        throw new IllegalArgumentException("serializer type is illegal. serializer type: '" + serializerType + "'");

    }

}
