package com.xuzhiguang.lightnat.common.codec;

import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

public class NatFrameDecoder extends LengthFieldBasedFrameDecoder {
    public NatFrameDecoder() {
        super(Integer.MAX_VALUE, 0, 4, 0, 4);
    }
}
