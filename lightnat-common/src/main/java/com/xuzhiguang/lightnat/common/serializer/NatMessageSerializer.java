package com.xuzhiguang.lightnat.common.serializer;


public interface NatMessageSerializer {

    byte[] serialize(Object obj);

    <T> T deserialize(byte[] bytes, Class<T> cls);

}
