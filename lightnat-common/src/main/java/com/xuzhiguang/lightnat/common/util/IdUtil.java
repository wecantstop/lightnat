package com.xuzhiguang.lightnat.common.util;

import cn.hutool.core.lang.Snowflake;

public class IdUtil {

    private static Snowflake snowflake = cn.hutool.core.util.IdUtil.getSnowflake(1, 1);

    public static long nextId() {
        return snowflake.nextId();
    }

}
