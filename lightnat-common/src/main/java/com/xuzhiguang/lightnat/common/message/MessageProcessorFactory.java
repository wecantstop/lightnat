package com.xuzhiguang.lightnat.common.message;

import java.util.LinkedHashMap;
import java.util.Map;

public class MessageProcessorFactory {

    private final Map<Short, MessageProcessor> processorMap = new LinkedHashMap<>();

    private MessageProcessorFactory() {

    }

    public static class Builder {

        private final MessageProcessorFactory factory;

        public Builder() {
            this.factory = new MessageProcessorFactory();
        }

        public Builder addProcessor(Short key, MessageProcessor processor) {
            this.factory.processorMap.put(key, processor);
            return this;
        }
        public MessageProcessorFactory build() {
            return this.factory;
        }
    }

    public MessageProcessor get(Short key) {
        for (Map.Entry<Short, MessageProcessor> entry : this.processorMap.entrySet()) {
            if (entry.getKey().equals(key)) {
                return entry.getValue();
            }
        }
        return null;
    }
}
