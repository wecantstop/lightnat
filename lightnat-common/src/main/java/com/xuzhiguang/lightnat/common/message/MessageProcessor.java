package com.xuzhiguang.lightnat.common.message;

import io.netty.channel.ChannelHandlerContext;

public interface MessageProcessor {

    void process(ChannelHandlerContext ctx, NatMessage natMessage);

}
