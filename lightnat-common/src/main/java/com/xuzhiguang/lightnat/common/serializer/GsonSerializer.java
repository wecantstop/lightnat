package com.xuzhiguang.lightnat.common.serializer;

import com.google.gson.Gson;

import java.nio.charset.StandardCharsets;

public class GsonSerializer implements NatMessageSerializer {

    private final Gson gson = new Gson();

    @Override
    public byte[] serialize(Object obj) {
        return gson.toJson(obj).getBytes();
    }

    @Override
    public <T> T deserialize(byte[] bytes, Class<T> clazz) {
        return gson.fromJson(new String(bytes, StandardCharsets.UTF_8), clazz);
    }
}
