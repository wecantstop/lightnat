# lightnat (内网穿透工具)


## 介绍

lightnat 是使用 netty 开发的一个内网穿透工具。可以使内网的服务在互联网上访问。


## 使用方法

lightnat 分为 server 端和 client 端。

server 端需要部署在拥有公网 ip 的机器上（例如：阿里云、腾讯云、华为云等等）

client 端部署在内网

### 1. 环境准备

lightnat 依赖 java 环境运行。需要事先安装 JDK 1.8

### 2. 编译打包

```shell
git clone https://gitee.com/wecantstop/lightnat
cd lightnat
mvn clean package -Dmaven.test.skip=true
cd distribution
```

文件树如下：
```shell
├── lightnat-client
│   ├── bin
│   │   ├── shutdown.cmd
│   │   ├── shutdown.sh
│   │   ├── startup.cmd
│   │   └── startup.sh
│   ├── conf
│   │   ├── client.properties
│   │   ├── logback-test.xml
│   │   └── logback.xml
│   └── target
│       └── lightnat-client.jar
└── lightnat-server
    ├── bin
    │   ├── shutdown.cmd
    │   ├── shutdown.sh
    │   ├── startup.cmd
    │   └── startup.sh
    ├── conf
    │   ├── application.yaml
    │   └── logback-spring.xml
    └── target
        └── lightnat-server.jar
```

### 3. 部署 server

application.yaml 配置文件
```yaml
spring:
  application:
    name: lightnat
server:
  port: 8080
  ## server api 访问地址，用来管理 token 等信息。不要暴露出去
  address: 127.0.0.1

natserver:
  transfer:
    bind-host: '*'
    bind-port: 1002
  control:
    bind-host: '*'
    bind-port: 1003
```

开启服务
```shell
sh bin/startup.sh
```

关闭服务
```shell
sh bin/shutdown.sh
```

api 接口

```shell
# 添加 client
POST http://localhost:8080/client/add

# 请求体
{
    "name": "test",
    "expire_time": 4070880000000
}

# 返回信息
{
    "code": 0,
    "msg": "success",
    "data": {
        "id": 1405408748230545408,
        "token": "c1pjvvpo3c1hd1leb7zaeyaxsm907zcy",
        "name": "test",
        "expire_time": 4070880000000
    }
}
```


```shell
# 获取 client 列表
GET http://localhost:8080/client/list

# 返回信息
{
    "code": 0,
    "msg": "success",
    "data": {
        "list": [
            {
                "id": 1405408748230545408,
                "token": "c1pjvvpo3c1hd1leb7zaeyaxsm907zcy",
                "name": "test",
                "expire_time": 4070880000000,
                "is_online": false
            }
        ]
    }
}
```


```shell
# 添加 proxy
POST http://localhost:8080/proxy/add

# 请求体
{
    "client_id": 1405408748230545408,
    "proxy_type": "HTTP",  // TCP HTTP
    "proxy_host": "127.0.0.1",
    "proxy_port": 1101,
    "source_host": "192.168.3.11",
    "source_port": 8080
}

# 返回信息
{
    "code": 0,
    "msg": "success",
    "data": {
        "id": 1405408748230545408,
        "token": "c1pjvvpo3c1hd1leb7zaeyaxsm907zcy",
        "name": "test",
        "expire_time": 4070880000000
    }
}
```


```shell
# 获取 proxy 列表
GET http://localhost:8080/proxy/list?clientId=1405408748230545408

# 返回信息
{
    "code": 0,
    "msg": "success",
    "data": {
        "list": [
            {
                "id": 1405488305784623104,
                "client_id": 1405408748230545408,
                "proxy_type": "TCP",
                "proxy_host": "127.0.0.1",
                "proxy_port": 1102,
                "source_host": "192.168.3.16",
                "source_port": 3306
            },
            {
                "id": 1405513339039256576,
                "client_id": 1405408748230545408,
                "proxy_type": "HTTP",
                "proxy_host": "127.0.0.1",
                "proxy_port": 1101,
                "source_host": "192.168.3.11",
                "source_port": 8080
            }
        ]
    }
}
```


### 4. 部署 client

client.properties 配置文件
```properties
# server 公网地址
natclient.server.host=127.0.0.1
natclient.server.control.port=1003
natclient.server.transfer.port=1002
# client token
natclient.server.token=c1pjvvpo3c1hd1leb7zaeyaxsm907zcy
```

开启服务
```shell
sh bin/startup.sh
```

关闭服务
```shell
sh bin/shutdown.sh
```

### 5. 访问服务

proxy_host,proxt_port 和 source_host,source_port 是一一对应的。

例如原本访问的是 http 服务。

> http://192.168.3.11:8080

现在替换成

> http://127.0.0.1:1101


## 开源许可

本项目采用 GPL 开源协议
